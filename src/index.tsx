import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from "react-redux";
import { BrowserRouter } from 'react-router-dom';
import { CssBaseline, createTheme, ThemeProvider } from '@mui/material';
import { store } from "./app/store";
import App from './App';
import './index.css';

const theme = createTheme({
  palette: {
    mode: "light",
    background: {
      default: "#fafafa",
    },
    primary: {
      main: "#00796b",
    },
    secondary: {
      main:"#2A3132"
    },
  },
})

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
          <BrowserRouter>
              <CssBaseline />
              <App />
          </BrowserRouter>
      </ThemeProvider>
    </Provider>
  </React.StrictMode>
);

