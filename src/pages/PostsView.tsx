import {  useEffect } from 'react';
import { Container } from '@mui/material';
import { useAppDispatch, useAppSelector } from '../app/hook';
import { ModalPostsCreate } from '../components/Modal/Modal-post-create';
import { HeaderPosts } from '../components/Header/Header-posts';
import { TablePosts } from "../components/Table/Table-posts"
import { getPostsAsync } from '../components/posts/posts-thunk';

export const PostsView = () => {
  const dispatch = useAppDispatch(); 
  const listPosts = useAppSelector((store) => store.posts.listPosts);

  useEffect(() => {
    dispatch(getPostsAsync());
  }, [])
  return (
    <Container maxWidth="xl" color="default">
      <HeaderPosts/>
      <ModalPostsCreate />
      <TablePosts listPosts={listPosts}/>
    </Container>
  )
}