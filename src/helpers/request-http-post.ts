import axios from "axios";
import { IRequestParamsPost } from "../interfaces/requests.interface";


const URL = process.env.REACT_APP_API_URL as  string; 

export async function requestHttpPost(params: IRequestParamsPost) {
  try {
    const response = await axios.post(URL, {
      userId: params.userId,
      title: params.title,
      body: params.body
    })
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
}