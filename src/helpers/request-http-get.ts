import axios from "axios";

const URL = process.env.REACT_APP_API_URL as string; 

export async function requestHttpGet() {
  try {
    const response = await axios.get(URL)
    return response.data;    
  } catch (e) {
    console.error(e);
    return e;
  }
}