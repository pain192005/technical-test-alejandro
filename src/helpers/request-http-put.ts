import axios from "axios";
import { RequestParamsPut } from "../interfaces/requests.interface";

const URL = process.env.REACT_APP_API_URL; 

export async function requestHttpPut(params: RequestParamsPut) {
  try {
    const endpoint = '/' + params.id;
    const response = await axios.put(URL + endpoint, {
      id: params.id,
      userId: params.userId,
      title: params.title,
      body: params.body
    });
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
}