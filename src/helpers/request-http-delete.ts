import axios from "axios";

const URL = process.env.REACT_APP_API_URL; 

export async function requestHttpDelete(id: string) {
  try {
    const endpoint = '/' + id;
    const response = await axios.delete(URL + endpoint);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
}