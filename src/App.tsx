import './App.css';
import { Routes, Route } from 'react-router-dom';
import { PostsView } from './pages/PostsView';
import { Container } from '@mui/material';

function App() {
  return (  
    <Container>
      <Routes>
        <Route path='/' element={<PostsView/>} />
      </Routes>
    </Container>
  );
}

export default App;
