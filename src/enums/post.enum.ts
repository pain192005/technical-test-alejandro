export enum EstatePage {
  LOADING = "loading",
  COMPLETED = "complete",
  FAILED = "failed"
};

export enum EtypeForm {
  EDIT = "edit",
  CREATE = "create",
  DELETE = "delete",
}