import { 
  FormControl, 
  TextField, 
  Container,
  Button, 
  Stack} from '@mui/material';
import { useForm } from "react-hook-form";
import { useAppDispatch, useAppSelector } from '../../app/hook';
import { Iform } from '../../interfaces/posts.interface';
import { modal } from '../posts/posts-slice';
import { putPostsAsync } from '../posts/posts-thunk';

export default function FormPostsEdit() {
  const { register, handleSubmit, formState: { errors } } = useForm<Iform>();
  const dispatch = useAppDispatch(); 
  const postUpdate = useAppSelector(store => store.posts.modal.postUpdate);
  
  const submitForm = (data: any) => {
    dispatch(putPostsAsync({...data, id: postUpdate?.id}));
    dispatch(modal({ openModal: false }));
  }

  return (
    <FormControl component="form" onSubmit={handleSubmit(submitForm)}>
      <Container maxWidth="md">
        <TextField 
          id="standard-full-width" 
          label="Titulo" 
          variant="outlined" 
          defaultValue={postUpdate?.title} 
          fullWidth
          {...register("title", { required: true })}
          sx={{ margin: "15px  0px"}}
        /> 
          {errors.title && 
            <small style={{color: "#00796b"}}>
              Este campo es requerido
            </small>
          }
        <TextField 
          id="outlined-basic" 
          label="Descripción" 
          multiline variant="outlined" 
          defaultValue={postUpdate?.body}
          fullWidth
          {...register("body", { required: true })}
          sx={{ margin: "15px  0px"}}
        />
          {errors.body && 
            <small style={{color: "#00796b"}}>
              Este campo es requerido
            </small>
          }
        <Stack spacing={2} direction="row" sx={{marginTop: "10px"}}>
          <Button 
          type="submit"
          variant="contained" 
          color="primary"
          sx={{ marginRight: "10px" }} 
        >
          Actualizar
          </Button>
          <Button 
            variant="contained"
            color="secondary" 
            onClick={() => dispatch(modal({openModal: false}))}
          >
            Cancelar
          </Button>
        </Stack>
      </Container>
    </FormControl>  
  )
}