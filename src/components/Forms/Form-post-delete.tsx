import { 
  Button, 
  Stack,
  Container,
  Box } from '@mui/material';
import { useAppDispatch, useAppSelector } from '../../app/hook';
import { modal } from '../posts/posts-slice'; 
import { deletePostsAsync } from '../posts/posts-thunk';

export default function FormPostsDelete() {
  const dispatch = useAppDispatch(); 
  const idDelete = useAppSelector(store => store.posts.modal.idDelete) as number;
  const deletePost = () => {
    dispatch(deletePostsAsync(idDelete.toString()))
    dispatch(modal({openModal: false}))
  }

  return (
    <Container sx={{ textAlign: "center" }}>
      <Box
        component="img"
        sx={{
          width: "200px"
        }}
        src={require("./../../img/deletePost.png")}
      />
      <Stack spacing={3} display="block" direction="row">
        <Button 
          type="submit"
          variant="contained" 
          size='large'
          onClick={() => deletePost()}
        >
          Eliminar
        </Button>
        <Button 
          variant="contained"
          color="secondary" 
          size='large'
          onClick={() => dispatch(modal({openModal: false}))}
        >
          Cancelar
        </Button>
      </Stack>
    </Container>
  );
}