import { 
  FormControl, 
  TextField, 
  Container,
  Button, 
  Stack} from '@mui/material';
import { useForm } from "react-hook-form";
import { useAppDispatch } from '../../app/hook';
import { Iform } from '../../interfaces/posts.interface';
import { modal } from '../posts/posts-slice'; 
import { postPostsAsync } from '../posts/posts-thunk';

export default function FormPostsCreate() {
  const { register, handleSubmit, formState: { errors }} = useForm<Iform>();
  const dispatch = useAppDispatch(); 
  const submitForm = (data: any) =>  {
    dispatch(postPostsAsync(data));
    dispatch(modal({openModal: false}));
  }

  return (
    <Container >
      <FormControl component="form" onSubmit={handleSubmit(submitForm)}>
        <Container >
          <TextField 
            id="standard-full-width" 
            label="Título" 
            variant="outlined" 
            fullWidth
            sx={{ margin: "10px  0px"}} 
            {...register("title", { required: true })}  //Fixing post insertion error
          /> 
            {errors.title && 
              <small style={{color: "#00796b"}}>
                Este campo es requerido
              </small>
            }
          <TextField 
            id="outlined-basic" 
            label="Descripción" 
            multiline variant="outlined"
            fullWidth
            sx={{ margin: "15px  0px"}}
            {...register("body", { required: true })}
          />
            {errors.body && 
              <small style={{color: "#00796b", display: "block", marginBottom: "10px" }}>
                Este campo es requerido
              </small>
            }
          <Stack spacing={2} direction="row">
            <Button 
              type="submit"
              variant="contained" 
              color="primary" 
              size='large'
            >
              Crear
            </Button>
            <Button 
              variant="contained"
              color="secondary" 
              size='large'
              onClick={() => dispatch(modal({openModal: false}))}
            >
              Cancelar
            </Button>
          </Stack>
        </Container>
      </FormControl>  
    </Container>
  );
}