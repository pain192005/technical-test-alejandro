import { 
  Container, 
  Table, 
  TableBody, 
  TableCell, 
  TableContainer, 
  TableHead, 
  TableRow, 
  Paper,
  Button, 
  Stack} from '@mui/material';
import ModalPosts from '../Modal/Modals-posts';
import { useAppDispatch } from '../../app/hook';
import { modal } from '../posts/posts-slice';
import { IPosts } from '../../interfaces/posts.interface';
import { EtypeForm } from '../../enums/post.enum';

export function TablePosts(props: { listPosts: IPosts[] } ) {
  const dispatch = useAppDispatch(); 
  const openModalEdit = (post: IPosts) => dispatch(modal({openModal: true, typeForm: EtypeForm.EDIT ,postUpdate: post}));
  const openModalDelete = (id: number) => dispatch(modal({openModal: true, typeForm: EtypeForm.DELETE, idDelete: id}));

  return (  
    <Container>
      <TableContainer component={Paper} elevation={3} color="secondary">
        <Table padding="normal">
          <TableHead sx={{ backgroundColor: "#2A3132" }} >
            <TableRow>
              <TableCell sx={{color: "white", textAlign: 'center'}}>N°</TableCell>
              <TableCell sx={{color: "white", textAlign: 'center'}}>Título</TableCell>
              <TableCell sx={{color: "white", textAlign: 'center'}}>Descripción</TableCell>
              <TableCell sx={{color: "white", textAlign: 'center'}}>Acciones</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            { props.listPosts.map((post, index) => ( 
              <TableRow key={post.id}>
                <TableCell>{index + 1}</TableCell>
                <TableCell>{post.title}</TableCell>
                <TableCell>{post.body}</TableCell>
                <TableCell>
                  <Stack spacing={2} direction="row">
                    <Button variant="contained" color='primary' onClick={() => openModalEdit(post)}>
                      Editar
                    </Button>
                    <Button variant="contained" color='secondary' onClick={() => openModalDelete(post.id)}>
                      Eliminar
                    </Button>
                  </Stack>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Container>
        <ModalPosts />
      </Container>
    </Container>
  )
}