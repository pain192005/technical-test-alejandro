import { 
  Container, 
  Button, 
  Typography, 
  Stack, 
  Paper} from '@mui/material';
import { useAppDispatch } from '../../app/hook';
import { modal } from '../posts/posts-slice';
import { EtypeForm } from '../../enums/post.enum';

export function ModalPostsCreate() {
  const dispatch = useAppDispatch();
  const openModalCreate = () => dispatch(modal({openModal: true, typeForm: EtypeForm.CREATE})); 

  return (
    <Container 
      component={Paper}
      maxWidth="sm" 
      sx={{ 
        borderRadius: "16px", 
        marginBottom: "30px",
        padding: "15px", 
        boxShadow: 6, 
      }}
    >
      <Stack spacing={2} direction="column"> 
        <Typography variant="h6" textAlign="center">
          ¿CREAR PUBLICACIÓN?
        </Typography>
        <Button variant="contained" onClick={() => openModalCreate()}>
          CREAR
        </Button>
      </Stack>
    </Container>
  );
}
