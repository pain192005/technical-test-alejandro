import { Modal, Container, Fade } from '@mui/material';
import { useAppSelector } from '../../app/hook';
import FormPostsCreate from '../Forms/Form-post-create';
import FormPostsDelete from '../Forms/Form-post-delete';
import FormPostsEdit  from '../Forms/Form-post-edit';
import HeaderPostsModal from '../Header/Header-posts-modal';
import { EtypeForm } from '../../enums/post.enum';

export default function ModalPosts() {
  const { openModal, typeForm } = useAppSelector((store) => store.posts.modal);
  
  return (
    <Container maxWidth="xs">
      <Modal 
        sx={{ display: "flex", justifyContent: "center", alignItems: 'center', }}
        open={openModal}
      >
        <Fade in={openModal}>
          <Container maxWidth="md" sx={{ backgroundColor: "white",  padding: "20px", borderRadius: "16px" }}>
           <HeaderPostsModal title={"¿ESTÁS SEGURO?"}/>
            { typeForm === EtypeForm.CREATE && <><FormPostsCreate/></> } 
            { typeForm === EtypeForm.EDIT && <><FormPostsEdit /></> } 
            { typeForm === EtypeForm.DELETE && <><FormPostsDelete/></> }
          </Container>
        </Fade>
      </Modal>
    </Container>
  );
}
