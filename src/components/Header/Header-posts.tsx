import { 
  AppBar, 
  Toolbar, 
  Typography, 
  Container } from '@mui/material';


export function HeaderPosts() {
  return (
    <Container sx={{ marginBottom: "100px"}}>
      <AppBar component="nav" color='primary'>
        <Toolbar>
          <Typography variant="h4">
            Registro De Publicaciones
          </Typography>
        </Toolbar>
      </AppBar>
    </Container>
  )
}
