import { 
  Typography, 
  Container } from '@mui/material';

export default function HeaderPostsModal(props: { title: string }) {

  return (
    <Container 
      sx={{ 
        backgroundColor: "#00796b",
        color: "#fafafa",
        marginBottom: "10px", 
        padding: "10px", 
        borderRadius: "15px", 
        textAlign: "center" 
      }} >
      <Typography variant="h5" color="inherit" >
        { props.title }
      </Typography>
    </Container>
  )
}