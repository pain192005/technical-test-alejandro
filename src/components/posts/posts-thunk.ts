import { createAsyncThunk } from '@reduxjs/toolkit';
import { requestHttpGet } from '../../helpers/request-http-get';
import { requestHttpPost } from '../../helpers/request-http-post';
import { requestHttpPut } from '../../helpers/request-http-put';
import { requestHttpDelete } from '../../helpers/request-http-delete';
import { IRequestParamsPost, RequestParamsPut } from '../../interfaces/requests.interface';
import { IPosts } from '../../interfaces/posts.interface';

export const getPostsAsync = createAsyncThunk(
  'posts/getPostsThunk',
  async () => {
    const getApiPosts = await requestHttpGet();
    const response = getApiPosts.filter((post: IPosts) => post.id <= 10);
    return response;
  }
)

export const postPostsAsync = createAsyncThunk(
  'posts/postPostsAsync',
  async (createPost: IRequestParamsPost) => {
    const postApiPosts = await requestHttpPost(createPost);
    return postApiPosts;
  }
)

export const putPostsAsync = createAsyncThunk(
  'posts/putPostsAsync',
  async (updatePost: RequestParamsPut) => {
    const putApiPosts = await requestHttpPut(updatePost);
    return putApiPosts;
  }
)

export const deletePostsAsync = createAsyncThunk(
  'posts/deletePostsAsync',
  async (id: string) => {
    const deleteApiPosts = await requestHttpDelete(id);
    return deleteApiPosts;
  }
)