import { createSlice } from "@reduxjs/toolkit";
import { deletePostsAsync, getPostsAsync, postPostsAsync, putPostsAsync } from "./posts-thunk";
import { EstatePage, EtypeForm } from "../../enums/post.enum";
import { IinitialState } from "../../interfaces/posts.interface";

const initialStatePosts: IinitialState = {
  listPosts: [],
  statePage: null,
  modal: {
    openModal: false,
    typeForm: null,
    postUpdate: null,
    idDelete: null, 
  },
};

const PostsListSlice = createSlice({
  name: "postsList",
  initialState: initialStatePosts,
  reducers: {
    modal(state, action) {
      state.modal.openModal = action.payload.openModal;
      state.modal.typeForm = action.payload.typeForm;
      if (state.modal.typeForm === EtypeForm.EDIT) state.modal.postUpdate = action.payload.postUpdate;
      if (state.modal.typeForm === EtypeForm.DELETE) state.modal.idDelete = action.payload.idDelete;
    },
  },
  extraReducers:(builder) => {
    builder
      .addCase(getPostsAsync.pending, (state) =>  {
        state.statePage = EstatePage.LOADING;
      })
      .addCase(getPostsAsync.fulfilled, (state, action) => {
        state.statePage = EstatePage.COMPLETED;
        state.listPosts = action.payload;
      })
      .addCase(getPostsAsync.rejected, (state) => {
        state.statePage = EstatePage.FAILED;
      })

    builder
      .addCase(postPostsAsync.pending, (state) =>  {
        state.statePage = EstatePage.LOADING;
      })
      .addCase(postPostsAsync.fulfilled, (state, action) => {
        state.statePage = EstatePage.COMPLETED;
        state.listPosts?.push(action.payload);
      })
      .addCase(postPostsAsync.rejected, (state) => {
        state.statePage = EstatePage.FAILED;
      })

    builder
      .addCase(putPostsAsync.pending, (state) =>  {
        state.statePage = EstatePage.LOADING;
      })
      .addCase(putPostsAsync.fulfilled, (state, action) => {
        state.statePage = EstatePage.COMPLETED;
        const indexPost = state.listPosts?.findIndex(post => post.id === action.payload.id);
        state.listPosts[indexPost] = {
          ...action.payload,
          id: state.listPosts[indexPost].id
        }
      })
      .addCase(putPostsAsync.rejected, (state) => {
        state.statePage = EstatePage.FAILED;
      })

    builder
      .addCase(deletePostsAsync.pending, (state) =>  {
        state.statePage = EstatePage.LOADING;
      })
      .addCase(deletePostsAsync.fulfilled, (state, action) => {
        state.statePage = EstatePage.COMPLETED;
        const updateList = state.listPosts?.filter((post) => post.id !== parseInt(action.meta.arg))
        state.listPosts = updateList;
      })
      .addCase(deletePostsAsync.rejected, (state) => {
        state.statePage = EstatePage.FAILED;
      })
  }
})
export const { modal } = PostsListSlice.actions;
export default PostsListSlice.reducer;