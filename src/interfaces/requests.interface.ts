export interface IRequestParamsPost {
  userId: number,
  title: string,
  body: string,
}

export interface RequestParamsPut extends IRequestParamsPost {
  id: string
}
