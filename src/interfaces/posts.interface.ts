import { EstatePage, EtypeForm } from "../enums/post.enum";
import { IRequestParamsPost } from "./requests.interface";

export interface IinitialState{
  listPosts: IPosts[],
  statePage: EstatePage | null,
  modal: {
    openModal: boolean,
    typeForm: EtypeForm | null,
    postUpdate: IPosts | null,
    idDelete: number | null,
  }, 
}
export interface IPosts extends IRequestParamsPost {
  id: number
}
export interface Iform {
  title: string,
  body: string
}
