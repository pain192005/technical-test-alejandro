import { configureStore } from "@reduxjs/toolkit";
import listPostsReducer from "../components/posts/posts-slice";
export const store = configureStore({
    reducer: {
        posts: listPostsReducer
    }
})

export type RootState = ReturnType<typeof store.getState> 
export type AppDispatch = typeof store.dispatch